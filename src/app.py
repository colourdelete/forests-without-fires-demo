import os
from flask import Flask, flash, request, redirect, url_for, render_template
from werkzeug.utils import secure_filename
import forms
import image_demo

UPLOAD_FOLDER = '/path/to/the/uploads'
ALLOWED_EXTENSIONS = {'bmp', 'gif', 'jpg', 'jpeg', 'png', 'tiff', 'webp'}  # subset of Pillow-supported formats

app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.config['SECRET_KEY'] = '8c569b55-7262-4544-91ac-d401eeb9386f'


@app.route('/')
def index():
  return render_template('index.html')


@app.route('/demo', methods=['GET', 'POST'])
def demo():
  form = forms.DemoForm()
  if form.validate_on_submit():
    f = form.image.data
    filename = secure_filename(f.filename)
    path = os.path.join(app.instance_path, 'uploads', filename)
    os.makedirs(os.path.splitext(path)[0], exist_ok = True)
    f.save(path)
    return render_template('demo-result.html', result = image_demo.demo_image(path))
  return render_template('demo.html', form = form)



if __name__ == '__main__':
  app.run(host = '0.0.0.0', port = 80)
