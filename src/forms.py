from flask_wtf import FlaskForm
from flask_wtf.file import FileField, FileRequired, FileAllowed


class DemoForm(FlaskForm):
  image = FileField(validators = [FileRequired(), FileAllowed({'bmp', 'gif', 'jpg', 'jpeg', 'png', 'tiff', 'webp'}, 'Images only!')])
